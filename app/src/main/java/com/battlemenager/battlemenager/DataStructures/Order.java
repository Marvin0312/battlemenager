package com.battlemenager.battlemenager.DataStructures;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Marcin on 2018-02-24.
 */

public class Order {
    public String type;
    public LatLng location;

    public Order(String type, LatLng location) {
        this.type = type;
        this.location = location;
    }
}
