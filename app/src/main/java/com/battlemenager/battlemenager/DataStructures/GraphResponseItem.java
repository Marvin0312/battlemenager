package com.battlemenager.battlemenager.DataStructures;

import android.graphics.Bitmap;

/**
 * Created by Maciej Trzciński on 2018-02-09.
 */

public class GraphResponseItem {

    //public static final List<DummyContent.DummyItem> ITEMS = new ArrayList<DummyContent.DummyItem>();
    //public static final Map<String, DummyContent.DummyItem> ITEM_MAP = new HashMap<String, DummyContent.DummyItem>();
    public String Name;
    public String PhotoUrl;
    public Bitmap Userphoto;
    public String UserID;


    public GraphResponseItem (String name){
        this.Name = name;

    }
    public GraphResponseItem (String name, String id){
        this.Name = name;
        this.UserID = id;

    }
    public GraphResponseItem (String name,String id, String URL){
        this.Name = name;
        this.PhotoUrl = URL;
        this.UserID = id;

    }
    public GraphResponseItem (String name, Bitmap photo){
        this.Name = name;
        this.Userphoto = photo;

    }



}
