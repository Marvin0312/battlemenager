package com.battlemenager.battlemenager.DataStructures;

import android.graphics.Bitmap;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Maciej Trzciński on 2018-02-09.
 */

public class FirebaseKeyValueItem {

   public Bitmap photo = null;
   public String key;
   public String value; // tymczasowo string pozniej pewnie MAP
    LatLng position;

    public FirebaseKeyValueItem(String key, String value){
        this.key = key;
        this.value = value;

    }

}
