package com.battlemenager.battlemenager;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class HeatMapData extends Thread{

    private List<LatLng> heatMapPoints;
    private DatabaseReference heatmapReference;

    public HeatMapData(DatabaseReference heatmapReference) {
        this.heatmapReference = heatmapReference;
    }

    @Override
    public void run() {
        heatmapReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(long i = dataSnapshot.getChildrenCount() + 1; !heatMapPoints.isEmpty(); i++){
                    heatmapReference.child(String.valueOf(i)).setValue(heatMapPoints.get(0));
                    heatMapPoints.remove(0);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    public void setListPoints(List<LatLng> points){
        this.heatMapPoints = points;
    }

    public void RemoveHeatMap(){
        heatmapReference.removeValue();
    }
}
