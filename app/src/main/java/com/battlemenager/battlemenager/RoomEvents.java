package com.battlemenager.battlemenager;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.battlemenager.battlemenager.DataStructures.Order;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcin on 2018-02-25.
 */

public class RoomEvents extends Thread {
    private final static String TAG = "RoomEvents";
    private DatabaseReference membersReference, ordersReference, heatmapReference, polygonReference;
    private RoomEventsListener listener;
    private String userName;
    private Handler handler = new Handler(), sendData = new Handler();
    private Runnable runnable;
    private LatLng player;
    private List<LatLng> membersLocations = new ArrayList<LatLng>();
    private List<Order> ordersLocations = new ArrayList<Order>();
    private List<LatLng> polygonList = new ArrayList<LatLng>();
    private List<List<LatLng>> polygonlistList = new ArrayList<List<LatLng>>();
    private List<LatLng> heatmapList = new ArrayList<LatLng>();

    public RoomEvents(Context context, RoomEventsListener listener, String roomName, String userName) {
        this.listener = listener;
        this.userName = userName;
        DatabaseReference rootReference = FirebaseDatabase.getInstance()
                .getReference()
                .child(context.getString(R.string.AllRoomsRef))
                .child(roomName);

        membersReference = rootReference.child("members");
        ordersReference = rootReference.child("orders");
        heatmapReference = rootReference.child("heat");
        polygonReference = rootReference.child("poly");
    }

    @Override
    public void run() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                membersReference.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Log.d(TAG, "onDataChange: ");
                        membersLocations.clear();
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            String data = child.getValue().toString();
                            if (!data.equals("POZYCJA")) {
                                LatLng latLng = new LatLng(
                                    Double.parseDouble(data.substring(0,data.indexOf("|"))),
                                    Double.parseDouble(data.substring(data.indexOf("|")+1)));
                                if (userName.equals(child.getKey())){
                                    player = latLng;
                                } else {
                                    membersLocations.add(latLng);
                                }
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                ordersReference.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        ordersLocations.clear();
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            String data = child.getValue().toString();
                            LatLng latLng = new LatLng(
                                Double.parseDouble(data.substring(0,data.indexOf("_"))),
                                Double.parseDouble(data.substring(data.indexOf("_")+1)));
                            String type = child.getKey();
                            Order order = new Order(type.substring(0, type.indexOf("|")), latLng);
                            ordersLocations.add(order);
                        }
                        listener.onRoomEventsSuccess(ordersLocations, heatmapList, polygonlistList);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                heatmapReference.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                            heatmapList.clear();
                            for(DataSnapshot map : dataSnapshot.getChildren()) {
                                heatmapList.add(
                                    new LatLng(
                                        Double.parseDouble(map.child("latitude").getValue().toString()),
                                        Double.parseDouble(map.child("longitude").getValue().toString())
                                    )
                                );
                            }
                        listener.onRoomEventsSuccess(ordersLocations, heatmapList, polygonlistList);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                polygonReference.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        polygonlistList.clear();
                        for(DataSnapshot polygons : dataSnapshot.getChildren()){
                            polygonList = new ArrayList<LatLng>();
                            for(DataSnapshot polygon : polygons.getChildren()){
                                polygonList.add(
                                    new LatLng(
                                        Double.parseDouble(polygon.child("latitude").getValue().toString()),
                                        Double.parseDouble(polygon.child("longitude").getValue().toString())
                                    )
                                );
                            }
                            polygonlistList.add(polygonList);
                        }
                        listener.onRoomEventsSuccess(ordersLocations, heatmapList, polygonlistList);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });

        sendData.post(runnable = new Runnable() {
            @Override
            public void run() {
                listener.onMembersPositionChanged(membersLocations,player);
                sendData.postDelayed(runnable, 1000); //opóźnienie
            }
        });
    }

    public void RemoveOrder(final LatLng order) {
        Log.w(TAG, "RemoveOrder: " );
        ordersReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.w(TAG, "onDataChange: " + order);
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Log.w(TAG, "onDataChange: " + child.getKey());
                    String data = child.getValue().toString();
                    LatLng latLng = new LatLng(
                            Double.parseDouble(data.substring(0,data.indexOf("_"))),
                            Double.parseDouble(data.substring(data.indexOf("_")+1)));
                    Log.w(TAG, "onDataChange: " + latLng);
                    if(order.equals(latLng)){
                        Log.w(TAG, "onDataChange: remowing");
                        child.getRef().removeValue();
                        break;
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void RemovePolygon(final List<LatLng> poligonPoints) {
        polygonReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<LatLng> points = new ArrayList<>();
                for (DataSnapshot polygon : dataSnapshot.getChildren()) {
                    points.clear();
                    for(DataSnapshot point : polygon.getChildren()){
                        points.add(new LatLng(
                            Double.parseDouble(point.child("latitude").getValue().toString()),
                            Double.parseDouble(point.child("longitude").getValue().toString())));
                    }
                    if (poligonPoints.containsAll(points)){
                        Log.w(TAG, "onDataChange: removing");
                        polygon.getRef().removeValue();
                        break;
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public interface RoomEventsListener {
        void onMembersPositionChanged(List<LatLng> members, LatLng player);
        void onRoomEventsSuccess( List<Order> orders, List<LatLng> heatmaplist, List<List<LatLng>> polygonlistList);
    }
}

