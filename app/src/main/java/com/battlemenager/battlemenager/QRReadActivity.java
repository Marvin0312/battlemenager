package com.battlemenager.battlemenager;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;

public class QRReadActivity extends AppCompatActivity {

    private static final String TAG = "QRReadActivity";
    private static final int CAMERA_PERMISSION = 4;
    private SurfaceView cameraView;
    private BarcodeDetector barcodeDetector;
    private Detector.Processor<Barcode> barcodeProcessor;
    private CameraSource cameraSource;
    private DatabaseReference checkPass;
    private ProgressDialog progressDialog;
    private EditText editTextRoomName, editTextPassword;
    private String roomName, roomPass;
    private boolean back = false, exist = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_read);

        cameraView = findViewById(R.id.cameraPreview);
        editTextRoomName = findViewById(R.id.editTextRoomNameJoin);
        editTextPassword = findViewById(R.id.editTextPasswordJoin);
        checkPass = FirebaseDatabase.getInstance().getReference(getString(R.string.AllRoomsRef));

        findViewById(R.id.buttonJoin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog = ProgressDialog.show(QRReadActivity.this,
                        "Proszę czekać.",
                        "Sprawdzanie danych",
                        true);
                roomName = editTextRoomName.getText().toString();
                roomPass = editTextPassword.getText().toString();

                checkPass.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        progressDialog.dismiss();
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            if (roomName.equals(child.getKey()) && roomPass.equals(child.child("Password").getValue().toString())){
                                exist = true;
                                finish();
                                break;
                            }
                        }
                        if (!exist){
                            Toast.makeText(QRReadActivity.this, "Błędna nazwa lub hasło", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        progressDialog.dismiss();
                    }
                });
            }
        });

        barcodeDetector = new BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.QR_CODE)
                .build();
        cameraSource = new CameraSource
                .Builder(this, barcodeDetector)
                .setRequestedPreviewSize(200, 200)
                .setAutoFocusEnabled(true)
                .build();
        cameraView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(QRReadActivity.this,
                            new String[]{Manifest.permission.CAMERA},
                            CAMERA_PERMISSION);
                    return;
                }
                try {
                    cameraSource.start(cameraView.getHolder());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {}
            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                cameraSource.stop();
            }
        });

        barcodeProcessor = new Detector.Processor<Barcode>() {
            @Override
            public void release() {}
            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> qrcodes = detections.getDetectedItems();
                if(qrcodes.size() != 0)
                {
                    barcodeDetector.release();
                    editTextRoomName.post(new Runnable() {
                        @Override
                        public void run() {
                            Vibrator vibrator = (Vibrator)getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                            vibrator.vibrate(1000);
                            String data = qrcodes.valueAt(0).displayValue;
                            Log.w(TAG, "receiveDetections: " + data);
                            editTextRoomName.setText(data.substring(0,data.indexOf("|")));
                            editTextPassword.setText(data.substring(data.indexOf("|")+1));
                            barcodeDetector.setProcessor(barcodeProcessor);
                        }
                    });
                }
            }
        };

        barcodeDetector.setProcessor(barcodeProcessor);
    }

    @Override
    public void onBackPressed() {
        back = true;
        finish();
        super.onBackPressed();
    }

    @Override
    public void finish() {
        if (!back) {
            Intent data = new Intent();
            data.putExtra("RoomName", editTextRoomName.getText().toString());
            data.putExtra("Password", editTextPassword.getText().toString());
            setResult(RESULT_OK, data);
        } else {
            setResult(RESULT_CANCELED);
        }
        super.finish();
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case CAMERA_PERMISSION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    try {
                        cameraSource.start(cameraView.getHolder());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            break;
        }
    }
}
