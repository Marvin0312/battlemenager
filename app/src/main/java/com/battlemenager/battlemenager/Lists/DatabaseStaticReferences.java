package com.battlemenager.battlemenager.Lists;

import android.content.Context;

import com.battlemenager.battlemenager.DataStructures.GraphResponseItem;
import com.battlemenager.battlemenager.MainActivity;
import com.battlemenager.battlemenager.R;
import com.google.firebase.database.DatabaseReference;

/**
 * Created by Maciej Trzciński on 2018-02-19.
 */

public class DatabaseStaticReferences {

    public static String MyInvieRef(Context context,GraphResponseItem item){
      return  context.getResources().getString(R.string.MyInvRef, item.UserID);
    }
    public static String RoomRef(Context context,String name){
        return context.getResources().getString(R.string.MyInvRef, name);
    }
    public static String RoomMembersRef(Context context,DatabaseReference RoomRef ){
        String ref = RoomRef.getRef().toString();
       return context.getResources().getString(R.string.newRef,ref,context.getResources().getString(R.string.members));
        //context.getResources().getString(R.string.newRef,roomMembersRef,user.getDisplayName())
    }
    public static String RoomMembersDirectRef(Context context, String roomName, String InnerRef, String Key){
        return context.getResources().getString(R.string.MyRoomRefInner,roomName,InnerRef, Key);
    }

}
   // String roomRef = MainActivity.this.getResources().getString(R.string.MyRoomRef,name); // create room
   // String roomMembersRef = MainActivity.this.getResources().getString(R.string.newRef,roomRef,MainActivity.this.getResources().getString(R.string.members));
