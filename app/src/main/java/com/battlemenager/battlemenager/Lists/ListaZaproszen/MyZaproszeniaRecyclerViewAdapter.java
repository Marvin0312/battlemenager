package com.battlemenager.battlemenager.Lists.ListaZaproszen;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.battlemenager.battlemenager.DataStructures.FirebaseKeyValueItem;
import com.battlemenager.battlemenager.R;


import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link //DummyItem} and makes a call to the
 * specified {@link FragmentZaproszenia.OnListFragmentInteractionListener}.
 *
 */
public class MyZaproszeniaRecyclerViewAdapter extends RecyclerView.Adapter<MyZaproszeniaRecyclerViewAdapter.ViewHolder> {

    private final List<FirebaseKeyValueItem> mValues;
    private final FragmentZaproszenia.OnListFragmentInteractionListener mListener;
    private final FragmentZaproszenia.OnAcceptInvitationListener mAcceptListener;
    private final FragmentZaproszenia.OnRejectInvitationListener mRejectListener;

    public MyZaproszeniaRecyclerViewAdapter(List<FirebaseKeyValueItem> items,
                                            FragmentZaproszenia.OnListFragmentInteractionListener listener,
                                            FragmentZaproszenia.OnAcceptInvitationListener AcceptListener,
                                            FragmentZaproszenia.OnRejectInvitationListener RejectListener) {
        mValues = items;
        mListener = listener;
        mAcceptListener = AcceptListener;
        mRejectListener = RejectListener;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_zaproszenia, parent, false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mIdView.setText("Zostałeś zaproszony do grupy "+mValues.get(position).key);
       holder.mContentView.setText((R.string.invite2));
       holder.AcceptInv.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               mAcceptListener.onAcceptInvitation(holder.mItem);
           }
       });
       holder.RejectInv.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               mRejectListener.onRejectInvitation(holder.mItem);
           }
       });
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        final ImageButton AcceptInv;
        final ImageButton RejectInv;
        public FirebaseKeyValueItem mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.id);
            mContentView = (TextView) view.findViewById(R.id.content);
            AcceptInv = view.findViewById(R.id.AcceptInv);
            RejectInv = view.findViewById(R.id.RejectInv);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
