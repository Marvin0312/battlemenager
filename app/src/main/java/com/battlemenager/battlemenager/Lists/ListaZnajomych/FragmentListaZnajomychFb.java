package com.battlemenager.battlemenager.Lists.ListaZnajomych;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.battlemenager.battlemenager.DataStructures.GraphResponseItem;
import com.battlemenager.battlemenager.R;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestAsyncTask;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class FragmentListaZnajomychFb extends Fragment {

    //
    private static final String ARG_COLUMN_COUNT = "column-count";
    //
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private OnInviteFriendClickedListener mInviteFirendListener;
    JSONArray friendlist;
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */

    ArrayList<GraphResponseItem> friends;


    public FragmentListaZnajomychFb() {
    }


    private void GraphRequest(final RecyclerView recyclerView){

        if(friends == null){
            friends = new ArrayList<GraphResponseItem>();
        GraphRequestAsyncTask graphRequestAsyncTask = new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/me/friends",
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    @Override
                    public void onCompleted(GraphResponse response) {
                        try {
                            friendlist = response.getJSONObject().getJSONArray("data");
                            Log.w("LISTA",response.toString());
                            for(int i = 0; i<friendlist.length();i++){
                                friends.add(new GraphResponseItem(
                                        (friendlist.getJSONObject(i).getString("name")),friendlist.getJSONObject(i).getString("id")));
                            }
                        }
                        catch (JSONException e){
                            Log.w("LISTA","Lista error");
                            e.printStackTrace();
                        }
                        recyclerView.setAdapter(new ItemViewAdapterZnajomi(friends, mListener, mInviteFirendListener));
                    }
                }).executeAsync();
        }
        else {
            recyclerView.setAdapter(new ItemViewAdapterZnajomi(friends, mListener, mInviteFirendListener));
        }

    }

    //
    @SuppressWarnings("unused")
    public static FragmentListaZnajomychFb newInstance(int columnCount) {
        FragmentListaZnajomychFb fragment = new FragmentListaZnajomychFb();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

          //  GraphRequest();
        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lista_znajomych, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            GraphRequest(recyclerView);
          // recyclerView.setAdapter(new ItemViewAdapterZnajomi(friends, mListener));
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
        if(context instanceof OnInviteFriendClickedListener){
            mInviteFirendListener = (OnInviteFriendClickedListener)context;
        }
        else {
            throw new RuntimeException(context.toString() + " must implement OnInviteFriendClickedListener");

        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {

        void onListFragmentInteraction(GraphResponseItem item);
    }
    public interface OnInviteFriendClickedListener{
        void onInviteFriendClicked(GraphResponseItem item);
    }
}

