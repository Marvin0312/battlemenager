package com.battlemenager.battlemenager.Lists.ListaCzlonkow;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.battlemenager.battlemenager.DataStructures.FirebaseKeyValueItem;
import com.battlemenager.battlemenager.Lists.ListaCzlonkow.FragmentListaCzlonkow.OnListFragmentInteractionListener;
import com.battlemenager.battlemenager.R;
//import com.battlemenager.battlemenager.dummy.DummyContent.DummyItem;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link// DummyItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 *
 */
public class ItemViewAdapterCzlonkowie extends RecyclerView.Adapter<ItemViewAdapterCzlonkowie.ViewHolder> {

    private final List<FirebaseKeyValueItem> mValues;
    private final OnListFragmentInteractionListener mListener;

    public ItemViewAdapterCzlonkowie(List<FirebaseKeyValueItem> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_item_czlonkoiwe, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
     //   holder.mItem = mValues.get(position);
        holder.mItem = mValues.get(position);
       // holder.mIdView.setText(mValues.get(position).key); ///foto
        holder.mContentView.setText(mValues.get(position).key);


        holder.UsunCzlonka.setOnClickListener(new View.OnClickListener() {//TODO Dodać interfejs obslugi przycisku
            @Override
            public void onClick(View view) {

            }
        });

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
     //   public final TextView mIdView;
        public final TextView mContentView;
        final ImageButton UsunCzlonka;
        public FirebaseKeyValueItem mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
         //   mIdView = (TextView) view.findViewById(R.id.id);
            mContentView = (TextView) view.findViewById(R.id.content);
            UsunCzlonka = view.findViewById(R.id.UsunCzlonka);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
