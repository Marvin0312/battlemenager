package com.battlemenager.battlemenager.Lists.ListaZnajomych;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.battlemenager.battlemenager.DataStructures.GraphResponseItem;
import com.battlemenager.battlemenager.R;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link FragmentListaZnajomychFb.OnListFragmentInteractionListener}.
 *
 */
public class ItemViewAdapterZnajomi extends RecyclerView.Adapter<ItemViewAdapterZnajomi.ViewHolder> {

    private final List<GraphResponseItem> mValues;
    private final FragmentListaZnajomychFb.OnListFragmentInteractionListener mListener;
    private final FragmentListaZnajomychFb.OnInviteFriendClickedListener mInviteListener;


    public ItemViewAdapterZnajomi(List<GraphResponseItem> items,
                                  FragmentListaZnajomychFb.OnListFragmentInteractionListener listener,
                                  FragmentListaZnajomychFb.OnInviteFriendClickedListener inviteFriendClickedListener) {
        mValues = items;
        mListener = listener;
        mInviteListener = inviteFriendClickedListener;
    }


 //   ArrayAdapter adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,friends);
      //  listaZnajomych.setAdapter(adapter);

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_item_znajomi, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);
       // holder.mIdView.(mValues.get(position).PhotoUrl);
        holder.mContentView.setText(mValues.get(position).Name);

        holder.invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mInviteListener.onInviteFriendClicked(holder.mItem);

               // Toast.makeText(view.getContext(),"PRZYCISK DZIALA :O", Toast.LENGTH_SHORT).show();
            }
        });


        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageButton invite;
     //   public final ImageView mIdView;
        public final TextView mContentView;
        public GraphResponseItem mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
         //   mIdView =  view.findViewById(R.id.id);
            mContentView =  view.findViewById(R.id.content);
            invite = view.findViewById(R.id.DodajZnajomego);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
