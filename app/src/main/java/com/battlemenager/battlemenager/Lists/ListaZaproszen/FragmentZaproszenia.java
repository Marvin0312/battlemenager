package com.battlemenager.battlemenager.Lists.ListaZaproszen;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.battlemenager.battlemenager.DataStructures.FirebaseKeyValueItem;
import com.battlemenager.battlemenager.R;
import com.facebook.AccessToken;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class FragmentZaproszenia extends Fragment {


    private static final String ARG_COLUMN_COUNT = "column-count";

    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private OnAcceptInvitationListener mAcceptListener;
    private OnRejectInvitationListener mRejectListener;

    ArrayList<FirebaseKeyValueItem> zaproszenia = new ArrayList<FirebaseKeyValueItem>();
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public FragmentZaproszenia() {
    }

    //
    @SuppressWarnings("unused")
    public static FragmentZaproszenia newInstance(int columnCount) {
        FragmentZaproszenia fragment = new FragmentZaproszenia();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //zaproszenia.add(new FirebaseKeyValueItem("TEST", "TEST1"));
        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_zaproszenia_list, container, false);
       //
        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            //recyclerView.setAdapter(new MyZaproszeniaRecyclerViewAdapter(zaproszenia, mListener));
            GetData(recyclerView);

        }
        return view;
    }

    private void GetData(final RecyclerView recyclerView) {
        if(AccessToken.getCurrentAccessToken() != null) {
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference inviteRef = database.getReference(getActivity().getApplicationContext().getResources()
                    .getString(R.string.MyInvRef, AccessToken.getCurrentAccessToken().getUserId()));
            inviteRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    // zaproszenia = new ArrayList<FirebaseKeyValueItem>();
                    zaproszenia.clear();
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        Log.w("lista zaproszen", dataSnapshot.getRef() + "count " + dataSnapshot.getChildrenCount() + " child " + child.getKey() + " " + child.getValue());
                        zaproszenia.add(new FirebaseKeyValueItem(child.getKey(), child.getValue().toString()));
                    }
                    recyclerView.requestLayout();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            recyclerView.setAdapter(new MyZaproszeniaRecyclerViewAdapter(zaproszenia, mListener, mAcceptListener, mRejectListener));
        }
      //  recyclerView.requestLayout();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
        if(context instanceof OnAcceptInvitationListener){
            mAcceptListener = (OnAcceptInvitationListener) context;
        }else {
            throw new RuntimeException(context.toString()+" must implement OnAcceptInvitationListener");
        }
        if(context instanceof OnRejectInvitationListener){
            mRejectListener = (OnRejectInvitationListener) context;
        }else {
            throw new RuntimeException(context.toString()+" must implement OnAcceptInvitationListener");
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(FirebaseKeyValueItem item);
    }
    public interface OnAcceptInvitationListener{
        void onAcceptInvitation(FirebaseKeyValueItem item);
    }
    public interface OnRejectInvitationListener{
        void onRejectInvitation(FirebaseKeyValueItem item);
    }
}
