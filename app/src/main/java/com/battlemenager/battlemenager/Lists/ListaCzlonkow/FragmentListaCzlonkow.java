package com.battlemenager.battlemenager.Lists.ListaCzlonkow;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.battlemenager.battlemenager.DataStructures.FirebaseKeyValueItem;
import com.battlemenager.battlemenager.R;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class FragmentListaCzlonkow extends Fragment {

    //
    private static final String ARG_COLUMN_COUNT = "column-count";
    private static String TAG = "Lista czlonkowie";
    //
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    FirebaseDatabase database;
     List<FirebaseKeyValueItem> firebaseKeyValueItems = new ArrayList<FirebaseKeyValueItem>();

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public FragmentListaCzlonkow() {
    }

    //
    @SuppressWarnings("unused")
    public static FragmentListaCzlonkow newInstance(int columnCount) {
        FragmentListaCzlonkow fragment = new FragmentListaCzlonkow();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    public void InitMemberList(final RecyclerView recyclerView){
        database = FirebaseDatabase.getInstance();
        firebaseKeyValueItems = new ArrayList<FirebaseKeyValueItem>();
        TextView TVname = getActivity().findViewById(R.id.RoomTitle);
        String roomname = TVname.getText().toString();
        String roomRef = getActivity().getApplicationContext().getResources().getString(R.string.MyRoomRef,roomname); // create room
        String roomMembersRef = getActivity().getApplicationContext().getResources().getString(R.string.newRef,roomRef,getActivity().getApplicationContext().getResources().getString(R.string.members));
        //DatabaseReference roomMembersRef = database.getReference();
        DatabaseReference myRef = database.getReference(roomMembersRef);
        Log.w(TAG,"Czlonkowie init"+myRef.getRef());
        firebaseKeyValueItems.add(new FirebaseKeyValueItem("test","test"));

        recyclerView.setAdapter(new ItemViewAdapterCzlonkowie(firebaseKeyValueItems, mListener));
        //myRef = myRef.getParent();
       /**
        *  Nawet kurwa nie pytaj co tu zaszło ;)
        *  */
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                //parentSnapshot[0] = dataSnapshot;
                dataSnapshot.getRef().addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot childSnapshot, String s) {
                        Log.w(TAG,"MEMBER ADDED");
                        firebaseKeyValueItems = new ArrayList<FirebaseKeyValueItem>();
                        for(DataSnapshot child : dataSnapshot.getChildren()){
                            firebaseKeyValueItems.add(new FirebaseKeyValueItem(child.getKey(),child.getValue().toString()));
                        }
                        recyclerView.setAdapter(new ItemViewAdapterCzlonkowie(firebaseKeyValueItems, mListener));
                    }
                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    }
                    @Override
                    public void onChildRemoved(DataSnapshot childSnapshot) {
                        Log.w(TAG,"MEMBER REMOVED");
                        firebaseKeyValueItems = new ArrayList<FirebaseKeyValueItem>();
                        for(DataSnapshot child : dataSnapshot.getChildren()){
                            firebaseKeyValueItems.add(new FirebaseKeyValueItem(child.getKey(),child.getValue().toString()));
                        }
                        recyclerView.setAdapter(new ItemViewAdapterCzlonkowie(firebaseKeyValueItems, mListener));
                    }
                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //JSONArray czlonkowie = myRef.;



        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_czlonkowie, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            InitMemberList(recyclerView);
           // recyclerView.setAdapter(new ItemViewAdapterCzlonkowie(firebaseKeyValueItems, mListener));
        }
        return view;
    }

    //private void setRecyclerAdapter()


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {

        void onListFragmentInteraction(FirebaseKeyValueItem item);
    }
}
