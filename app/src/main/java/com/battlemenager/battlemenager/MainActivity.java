package com.battlemenager.battlemenager;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.battlemenager.battlemenager.DataStructures.FirebaseKeyValueItem;
import com.battlemenager.battlemenager.Lists.ListaZaproszen.FragmentZaproszenia;
import com.facebook.AccessToken;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.IdpResponse;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity
    implements View.OnClickListener,
        FragmentZaproszenia.OnListFragmentInteractionListener,
        FragmentZaproszenia.OnAcceptInvitationListener,
        FragmentZaproszenia.OnRejectInvitationListener{
    private final static String TAG = "MainActivity";
    private static final int SIGN_IN = 1001;
    private final static int QR_SCAN = 1002;
    private final static int USUN_POKUJ_Z_BAZY = 1003;
    public static final int  COARSE = 100;
    public static final int FINE = 101;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;
    private NavigationView menu;
    private ProgressDialog dialog;
    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private FirebaseDatabase database;
    private EditText roomName, roomPass;
    private TextView textViewEmail, textViewUsername;
    private static final List<String> listaUprawnienFacebook = new ArrayList<String>(){{
        add("email");
        add("user_friends");
        add("public_profile");
    }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (FirebaseAuth.getInstance().getCurrentUser() != null){
            InitMainUI();
        } else {
            LoginUI();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonJoin:
                JoinUI();
                break;
            case R.id.buttonNewRoom:
                NewRoomUI();
                break;
            case R.id.buttonCreate:
                CreateRoom(roomName.getText().toString(), roomPass.getText().toString());
                break;
            case R.id.buttonCancel:
                Cancel();
                break;
            default:
                break;
        }
    }

    private boolean checkLocationPermission(Activity thisActivity) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(thisActivity,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, FINE);
            ActivityCompat.requestPermissions(thisActivity, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION}, COARSE);
            return false;
        }
        else
            return true;

    }

    private void LoginUI() {
        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.GoogleBuilder().build(),
                new AuthUI.IdpConfig.FacebookBuilder()
                        .setPermissions(listaUprawnienFacebook)
                        .build());
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .setLogo(R.drawable.logo)
                        .setTheme(R.style.AppTheme)
                        .setIsSmartLockEnabled(false, true)
                        .build(),
                SIGN_IN);
    }

    private void InitMainUI() {
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        database = FirebaseDatabase.getInstance();

        CreateMenu();
        View headerMenu = menu.getHeaderView(0);
        textViewEmail = headerMenu.findViewById(R.id.TextViewEmail);
        textViewUsername = headerMenu.findViewById(R.id.TextViewUser);
        textViewEmail.setText(user.getEmail());
        textViewUsername.setText(user.getDisplayName());

        roomName = findViewById(R.id.editTextNewRoomName);
        roomPass = findViewById(R.id.editTextNewRoomPass);
        findViewById(R.id.buttonJoin).setOnClickListener(this);
        findViewById(R.id.buttonNewRoom).setOnClickListener(this);
        findViewById(R.id.buttonCreate).setOnClickListener(this);
        findViewById(R.id.buttonCancel).setOnClickListener(this);
    }

    private void JoinUI(){
        startActivityForResult(new Intent(MainActivity.this,QRReadActivity.class), QR_SCAN);
    }

    private void NewRoomUI(){
        findViewById(R.id.linearLayoutChoose).setVisibility(View.GONE);
        findViewById(R.id.linearLayoutNewRoom).setVisibility(View.VISIBLE);
        checkLocationPermission(this);
    }

    private void AcceptInvitation(final FirebaseKeyValueItem item){
        Log.w(TAG,"Dołączanie do grupy");
        final String roomRef = MainActivity.this.getResources().getString(R.string.MyRoomRef,item.key);
        final DatabaseReference room = database.getReference(roomRef);
        //room exists?
        room.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    String roomMembersRef = MainActivity.this.getResources().getString(R.string.newRef, roomRef, MainActivity.this.getResources().getString(R.string.members));

                    DatabaseReference myRef = database.getReference(MainActivity.this.getResources().getString(R.string.newRef, roomMembersRef, user.getDisplayName()));
                    Log.w(TAG, myRef.toString());
                    myRef.setValue("POZYCJA", new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                            if (databaseError != null)
                                Log.w(TAG, databaseError.getMessage());
                            JoinToRoom(item.key, item.value);
                        }
                    });
                    Cancel();
                }
                else {
                    Toast.makeText(getApplicationContext(),"Grupa już nie istnieje!",Toast.LENGTH_SHORT).show();
                }
                room.removeEventListener(this);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void CreateRoom(final String name, final String pass) {
        Log.d(TAG, "CreateRoom: " + name.length());
        dialog = ProgressDialog.show(MainActivity.this, "Proszę czekać.", "Tworzenie pokoju", true);
        final DatabaseReference reference = database.getReference(getString(R.string.AllRoomsRef));
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d(TAG, "onDataChange: ");
                if (dataSnapshot.child(name).exists()) {
                    Toast.makeText(MainActivity.this, "Nazwa pokoju jest już zajęta", Toast.LENGTH_LONG).show();
                }else {
                    reference.child(name).child("Password").setValue(pass);
                    JoinToRoom(name, pass);
                    Cancel();
                }
                dialog.dismiss();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private void JoinToRoom(@NonNull String roomName, String roomPass) {
        Intent intent = new Intent(getApplicationContext(), RoomActivity.class);
        intent.putExtra("roomName", roomName);
        intent.putExtra("roomPass", roomPass);
        if(checkLocationPermission(this))
        startActivityForResult(intent, USUN_POKUJ_Z_BAZY);
        else
            Toast.makeText(MainActivity.this, "Wymagane zezwolenie na dostęp do lokalizacji", Toast.LENGTH_SHORT).show();
    }

    private void Cancel() {
        roomName.setText("");
        roomPass.setText("");
        findViewById(R.id.linearLayoutChoose).setVisibility(View.VISIBLE);
        findViewById(R.id.linearLayoutNewRoom).setVisibility(View.GONE);
    }

    /** Obsluga main_menu */
    private void CreateMenu() {
        drawerLayout = findViewById(R.id.drawer);
        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.Open, R.string.Close);
        drawerLayout.addDrawerListener(toggle);
        menu = findViewById(R.id.main_menu);
        toggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        MenuItemListener();
    }

    public boolean onOptionsItemSelected(MenuItem item){
        if (toggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void MenuItemListener(){
        menu.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.NewRoom:
                        NewRoomUI();
                        break;
                    case R.id.Join:
                        JoinUI();
                        break;
                    case R.id.MenuSettings:
                        //Intent PreferencesActivity
                        break;
                    case R.id.BtDisconnect:
                        AuthUI.getInstance().signOut(getApplicationContext());
                        LoginUI();
                        break;
                    case R.id.Delete:
                        AuthUI.getInstance().delete(getApplicationContext());
                        LoginUI();
                        break;
                }
                drawerLayout.closeDrawers();
                return true;
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case FINE:
                if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                break;
            case COARSE:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                break;
        }
    }

    /***/
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case SIGN_IN:
                IdpResponse response = IdpResponse.fromResultIntent(data);
                // Successfully signed in
                if (resultCode == RESULT_OK) {
                    InitMainUI();
                } else {
                    // Sign in failed
                    if (response == null) {
                        // User pressed back button
                        return;
                    }
                    if (response.getErrorCode() == ErrorCodes.NO_NETWORK) {
                        Toast.makeText(getApplicationContext(), "Wystąpił błąd sieci", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (response.getErrorCode() == ErrorCodes.UNKNOWN_ERROR) {
                        Toast.makeText(getApplicationContext(), "Nieznany błąd", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                break;
            case QR_SCAN:
                if (resultCode == RESULT_OK) {
                    JoinToRoom(data.getStringExtra("RoomName"), data.getStringExtra("Password"));
                }
                break;
            case USUN_POKUJ_Z_BAZY:
                //DeleteRoom(); // jeszcze puste
                break;
        }
    }

    @Override
    public void onListFragmentInteraction(FirebaseKeyValueItem item) {
        Toast.makeText(getApplicationContext(),item.key+"|"+item.value+" KLIK!",Toast.LENGTH_SHORT).show();
        //usunac zapke z bazy
    }

    @Override
    public void onAcceptInvitation(FirebaseKeyValueItem item) {
        AcceptInvitation(item);
        //TODO USUN Z BAZY ( chyba że usuwamy tylko przy wyjściu z pokoju i zamknięciu pokoju aby na jednym zaproszeniu móc wracać wielokrotnie jesli pokój istnieje)
    }

    @Override
    public void onRejectInvitation(FirebaseKeyValueItem item) {
        Toast.makeText(getApplicationContext(),item.key+"|"+item.value+" Odrzucono zaproszenie!",Toast.LENGTH_SHORT).show();
        DatabaseReference myInv = database.getReference(MainActivity.this.getResources().getString(R.string.MyInvRef,AccessToken.getCurrentAccessToken().getUserId()));
        myInv.child(item.key).removeValue();
    }
}
