package com.battlemenager.battlemenager;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.battlemenager.battlemenager.DataStructures.Order;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.maps.android.heatmaps.HeatmapTileProvider;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MapsActivity extends FragmentActivity
        implements OnMapReadyCallback,
        GoogleMap.OnCameraMoveStartedListener,
        GoogleMap.OnMapLongClickListener,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnPolygonClickListener,
        RoomEvents.RoomEventsListener,
        Compass.CompassListener{

    private final static String TAG = "MapsActivity";
    private GoogleMap mMap;
    private TextView textViewDelay;
    private Button buttonCleanHeatMap;
    private ImageButton autocenter, drawHeat,drawPoly,drawEnd;
    private ImageView compassView;
    private CameraDelay cameraDelay;
    private ProgressDialog progressDialog;
    private RoomEvents roomEvents;
    private Compass compass;
    private HeatMapData heatMapData;
    private DatabaseReference orders, polygonsref;
    private String roomName, userName;
    private boolean cameraPause = false, move_map_available = false, poly = false, heat = false;
    Projection projection;
    final ArrayList<LatLng> lista= new ArrayList<>();
    List<Marker> listaMarkerow = new ArrayList<Marker>();

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        //get destination from intent
        Intent intent = getIntent();
        roomName = intent.getStringExtra("roomName");
        userName = intent.getStringExtra("userName");
        progressDialog = ProgressDialog.show(MapsActivity.this, "Proszę czekać.", "Ładowanie mapy", true);

        compassView = findViewById(R.id.Compass);
        autocenter = findViewById(R.id.Button3);
        drawHeat = findViewById(R.id.ButtonDrawHeatMap);
        drawPoly = findViewById(R.id.ButtonDrawPolygon);
        drawEnd = findViewById(R.id.ButtonDrawEnd);
        buttonCleanHeatMap = findViewById(R.id.buttonCleanHeatMap);
        textViewDelay = findViewById(R.id.textViewDelay);

        registerForContextMenu(textViewDelay);
        final FrameLayout fram_maps = findViewById(R.id.fram_maps);
        roomEvents = new RoomEvents(this, this, roomName, userName);
        orders = FirebaseDatabase.getInstance().getReference()
                .child(getString(R.string.AllRoomsRef)).child(roomName).child("orders");
        heatMapData = new HeatMapData(orders.getParent().child("heat"));
        polygonsref = orders.getParent().child("poly");
        EnableCompass();

        //region onClickListeners
        autocenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cameraPause){
                    cameraDelay.cancel(true);
                    textViewDelay.setText("");
                    cameraPause = false;
                }
            }
        });

        buttonCleanHeatMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                heatMapData.RemoveHeatMap();
            }
        });

        drawHeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HideButtons();
                heat = cameraPause = true;
                buttonCleanHeatMap.setVisibility(View.VISIBLE);
            }
        });
        drawPoly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HideButtons();
                poly = cameraPause = true;
            }
        });
        drawEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowButtons();
                buttonCleanHeatMap.setVisibility(View.GONE);
                poly = heat = cameraPause = false;
            }
        });
        //endregion

        fram_maps.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                int x_round = Math.round(motionEvent.getX());
                int y_round = Math.round(motionEvent.getY());
                //projection = mMap.getProjection();
                Point x_y = new Point(x_round, y_round);
                LatLng latLng = mMap.getProjection().fromScreenLocation(x_y);
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        lista.clear();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        lista.add(latLng);
                        break;
                    case MotionEvent.ACTION_UP:
                        if(poly){
                            new Handler().post(new Runnable() {
                                @Override
                                public void run() {
                                    polygonsref.child(Calendar.getInstance().getTime().toString()).setValue(lista);
                                }
                            });
                        }
                        if (heat) {
                            heatMapData.setListPoints(lista);
                            heatMapData.run();
                        }
                        break;
                }
                return move_map_available;
            }
        });
    }

    @Override
    protected void onDestroy() {
        roomEvents.interrupt();
        DisableCompass();
        super.onDestroy();
    }

    /**
     * Map
     */
    @Override
    @SuppressWarnings("MissingPermission")
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this,R.raw.my_map_style));
        mMap.setMyLocationEnabled(true);
        mMap.setMinZoomPreference(15.0f);
        mMap.setMaxZoomPreference(20.0f);
        //mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN); /** typ topograficzny **/
        mMap.setOnCameraMoveStartedListener(this);
        mMap.setOnMapLongClickListener(this);

    //    mMap.setInfoWindowAdapter(infoWindowAdapter); // Nie działa jak trzeba
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.setOnMarkerClickListener(this);
        mMap.setOnPolygonClickListener(this);
        roomEvents.run();
    }

    @Override
    public void onMapLongClick(final LatLng latLng) {
        PopupMenu popupMenu = new PopupMenu(MapsActivity.this, textViewDelay);
        popupMenu.getMenuInflater().inflate(R.menu.popup_menu, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                String location = String.valueOf(latLng.latitude)+ "_" + String.valueOf(latLng.longitude);
                switch (item.getItemId()) {
                    case R.id.Ally_Centre:
                        orders.child("Ally_Centre|"+ Calendar.getInstance().getTime()).setValue(location);
                        break;
                    case R.id.Ally_Transportation:
                        orders.child("Ally_Transportation|"+ Calendar.getInstance().getTime()).setValue(location);
                        break;
                    case R.id.Ally_Medic:
                        orders.child("Ally_Medic|"+ Calendar.getInstance().getTime()).setValue(location);
                        break;

                    case R.id.Enemy_Target:
                        orders.child("Enemy_Target|"+Calendar.getInstance().getTime()).setValue(location);
                        break;
                    case R.id.Enemy_Transportation:
                        orders.child("Enemy_Transportation|"+Calendar.getInstance().getTime()).setValue(location);
                        break;
                    case R.id.Enemy_Medic:
                        orders.child("Enemy_Medic|"+Calendar.getInstance().getTime()).setValue(location);
                        break;
                    default:
                        break;
                }
                return true;
            }
        });
        popupMenu.show();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
//        if (marker.isInfoWindowShown()){
//            marker.hideInfoWindow();
            String title = marker.getTitle();
            if (!title.equals("ally") ){
                Log.w(TAG, "onMarkerClick: usuwa");
                roomEvents.RemoveOrder(marker.getPosition());
                marker.remove();
            }
//        } else {
//            marker.showInfoWindow();
//        }
        return false;
    }


    @Override
    public void onPolygonClick(Polygon polygon) {
        roomEvents.RemovePolygon(polygon.getPoints());
    }

    /**
     * camera listener
     */
    @Override
    public void onCameraMoveStarted(int i) {
        Log.d(TAG, "onCameraMoveStarted: " + i);
        if (i == 1) {
            if (cameraPause){
                cameraDelay.cancel(true);
                textViewDelay.setText("");
            }
            cameraDelay = (CameraDelay) new CameraDelay().execute();
        }
    }

    /**
     * database listeners
     */
    @Override
    public void onMembersPositionChanged(List<LatLng> members, LatLng player) {
        for(Marker marker : listaMarkerow)
            marker.remove();
        listaMarkerow.clear();
        if (player != null){
            if(progressDialog.isShowing()) progressDialog.dismiss();
            listaMarkerow.add(mMap.addMarker(new MarkerOptions()
                .position(player)
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_player))
                .title("ally")));
            if (!cameraPause) GoToLocation(player);
        }
        for (LatLng member : members){
            listaMarkerow.add(mMap.addMarker(new MarkerOptions()
                .position(member)
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_ally))
                .title("ally")));
        }
    }

    @Override
    public void onRoomEventsSuccess( List<Order> orders, List<LatLng> listaheatmap, List<List<LatLng>> listaPoligonow ) {
        mMap.clear();
        for (Order order : orders){
            Marker marker = mMap.addMarker(new MarkerOptions()
                .position(order.location));
            if (order.type.equals("Ally_Centre")){
                marker.setTitle("Ally_Centre");
                marker.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_ally_centre));
            }
            else if (order.type.equals("Ally_Transportation")){
                marker.setTitle("Ally_Transportation");
                marker.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_ally_transport));
            }
            else if (order.type.equals("Ally_Medic")){
                marker.setTitle("Ally_Medic");
                marker.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_ally_medic));
            }

            else if (order.type.equals("Enemy_Target")){
                marker.setTitle("Enemy_Target");
                marker.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_enemy_target));
            }
            else if (order.type.equals("Enemy_Transportation")){
                marker.setTitle("Enemy_Transportation");
                marker.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_enemy_transport));
            }
            else if (order.type.equals("Enemy_Medic")){
                marker.setTitle("Enemy_Medic");
                marker.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_enemy_medic));
            }
        }
        if (!listaheatmap.isEmpty())
            Draw_Heat_Map(listaheatmap);
        for(List<LatLng> polygon : listaPoligonow){
            Draw_Polygon(polygon);
        }
    }

    /**
     * compass
     */
    private void EnableCompass() {
        Log.d(TAG, "EnableCompass: ");
        compassView.setVisibility(View.VISIBLE);
        compass = new Compass(this, this);
        compass.run();
    }

    @Override
    public void onCompassResult(float azimuth) {
        compassView.setRotation(azimuth);
    }

    private void DisableCompass() {
        compassView.setVisibility(View.GONE);
        compass.interrupt();
    }
    /**
     * my methods
     */
    public void Draw_Heat_Map(List<LatLng> heatmap){
        mMap.addTileOverlay(new TileOverlayOptions().tileProvider(new HeatmapTileProvider.Builder()
                .data(heatmap)
                .build()));
    }
    public void Draw_Polygon(List<LatLng> lista) {
        mMap.addPolygon(new PolygonOptions().addAll(lista)
                .strokeColor(0x100000ff)
                .strokeWidth(7)
                .fillColor(0x1f00ff00)
                .clickable(true));
    }

    private void HideButtons(){
        move_map_available = !move_map_available;
        drawHeat.setVisibility(View.GONE);
        drawPoly.setVisibility(View.GONE);
        autocenter.setVisibility(View.GONE);
        drawEnd.setVisibility(View.VISIBLE);
    }
    private void ShowButtons(){
        move_map_available = !move_map_available;
        drawHeat.setVisibility(View.VISIBLE);
        drawPoly.setVisibility(View.VISIBLE);
        autocenter.setVisibility(View.VISIBLE);
        drawEnd.setVisibility(View.GONE);
    }

    private void GoToLocation(LatLng latLng) {
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 17.0f);
        mMap.animateCamera(cameraUpdate);
    }

    private class CameraDelay extends AsyncTask<String, Integer, String> {
        @Override
        protected void onPreExecute() {
            cameraPause = true;
        }

        @Override
        protected String doInBackground(String... strings) {
            for (int i = 10; i > 0; i--) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                publishProgress(i);
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            textViewDelay.setText(values[0].toString());
        }

        @Override
        protected void onPostExecute(String s) {
            cameraPause = false;
            textViewDelay.setText("");
        }
    }
}
