package com.battlemenager.battlemenager;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.battlemenager.battlemenager.DataStructures.FirebaseKeyValueItem;
import com.battlemenager.battlemenager.DataStructures.GraphResponseItem;
import com.battlemenager.battlemenager.Lists.DatabaseStaticReferences;
import com.battlemenager.battlemenager.Lists.ListaZnajomych.FragmentListaZnajomychFb;
import com.battlemenager.battlemenager.Lists.ListaCzlonkow.FragmentListaCzlonkow;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

public class RoomActivity extends AppCompatActivity implements FragmentListaCzlonkow.OnListFragmentInteractionListener,
        FragmentListaZnajomychFb.OnListFragmentInteractionListener, FragmentListaZnajomychFb.OnInviteFriendClickedListener{

    private final static String TAG = "RoomActivity";
    ImageView QRImage;
    String roomName = "ErrorName", roomPass;
    TextView Title;
    TabHost mTabHost;
    public static final int MAPA = 199;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);
        final Intent intent = getIntent();
        roomName = intent.getStringExtra("roomName");
        roomPass = intent.getStringExtra("roomPass");
        Title = findViewById(R.id.RoomTitle);
        Title.setText(roomName);
       // checkLocationPermission(this);

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference(getString(R.string.AllRoomsRef));
        reference.child(roomName).child("members").child(FirebaseAuth.getInstance().getCurrentUser().getDisplayName()).setValue("POZYCJA");
        StartService();

        findViewById(R.id.DoMapy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mapa =new Intent( RoomActivity.this,MapsActivity.class);
                mapa.putExtra("roomName", roomName);
                mapa.putExtra("userName", FirebaseAuth.getInstance().getCurrentUser().getDisplayName());
                startActivityForResult(mapa,MAPA);
            }
        });

        QRImage = findViewById(R.id.QRcode);
        try {
           QRImage.setImageBitmap(GenerateQR(roomName + "|" + roomPass));
        } catch (WriterException e) {
            e.printStackTrace();
        }
        FragmentTabHost fragmentTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        fragmentTabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);
        fragmentTabHost.addTab(getTabSpec1(fragmentTabHost), FragmentListaCzlonkow.class, null);
        fragmentTabHost.addTab(getTabSpec2(fragmentTabHost), FragmentListaZnajomychFb.class, null);


        findViewById(R.id.CloseRoom).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(RoomActivity.this);
                builder.setTitle("Opuść grupę");
                builder.setIcon(R.mipmap.close_room);
                builder.setMessage("Czy chcesz opuścic tą grupę?")
                        .setCancelable(false)
                        .setPositiveButton("Tak", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //stopowanie serwisu
                                Intent servIntent = new Intent(getApplicationContext(), LocationService.class);
                                stopService(servIntent);
                                final DatabaseReference usunMember = FirebaseDatabase.getInstance().getReference(DatabaseStaticReferences.RoomMembersDirectRef
                                        (RoomActivity.this,
                                                roomName,
                                                RoomActivity.this.getResources().getString(R.string.members),
                                                FirebaseAuth.getInstance().getCurrentUser().getDisplayName().toString()
                                        ));

                                final DatabaseReference usunPokoj =  usunMember.getParent();
//                                usunMember.removeValue();
                                usunPokoj.addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        if( dataSnapshot.getChildrenCount()> 0){
                                            Log.w("USUN POKOJ", usunPokoj.toString());
                                            usunPokoj.removeEventListener(this);
                                            Log.w("CloseRoom","Opuszczono pokoj, ale nie usunieto grupy");
                                        }
                                        else {
                                            final DatabaseReference invited = usunMember.getParent().getParent().child(RoomActivity.this.getResources().getString(R.string.invitedFreinds));
                                            Log.w("ref ", invited.toString());
                                            invited.addValueEventListener(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    for(DataSnapshot item : dataSnapshot.getChildren()){
                                                       String key = item.getKey();
                                                       Log.w("USUWANIE ZAPROSZEN","USUWANIE "+key);
                                                       DatabaseReference usunZaproszenie = invited.getParent().getParent().getParent().child(RoomActivity.this.getResources().getString(R.string.Invitations));
                                                       usunZaproszenie.child(key).child(roomName).removeValue();
                                                    }
                                                    usunMember.getParent().getParent().removeValue();
                                                }
                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {

                                                }
                                            });

                                            usunPokoj.removeEventListener(this);
                                            Log.w("CloseRoom","Opuszczono pokoj i usunieto grupe");
                                        }
                                    }
                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                    }
                                });
                                Log.d(TAG, "onClick: ");
                                finish();
                            }
                        })
                        .setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        });
                builder.create();
                builder.show();
                }
            });


    }

    private TabHost.TabSpec getTabSpec1(FragmentTabHost tabHost) {
        return tabHost.newTabSpec("First Tab")
                .setIndicator("Członkowie",getResources().getDrawable(R.mipmap.invite_person));

    }

    private TabHost.TabSpec getTabSpec2(FragmentTabHost tabHost) {
        return tabHost.newTabSpec("Second Tab")
                .setIndicator("Zaproś", getResources().getDrawable(R.drawable.com_facebook_profile_picture_blank_portrait));
    }

    private Bitmap GenerateQR(String data) throws WriterException {
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        BitMatrix bitMatrix = multiFormatWriter.encode(data, BarcodeFormat.QR_CODE,400,400);
        int width = bitMatrix.getWidth();
        int height = bitMatrix.getHeight();
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                bitmap.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
            }
        }
        return bitmap;
       // imageView.setImageBitmap(bitmap);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
        }
    }

    @Override
    public void onBackPressed() {
        //nic

    }

    @Override
    public void onListFragmentInteraction(GraphResponseItem item) {
        Toast.makeText(getApplicationContext(),"KLIK!",Toast.LENGTH_SHORT).show();
      /*  FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference inviteRef = database.getReference(getApplicationContext().getResources()
                .getString(R.string.MyInvRef, item.UserID));
        inviteRef.child(roomName).setValue(roomPass);*/
    }

    @Override
    public void onListFragmentInteraction(FirebaseKeyValueItem item) {
       // Toast.makeText(getApplicationContext(),"KLIK!",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onInviteFriendClicked(GraphResponseItem item) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference inviteRef = database.getReference(DatabaseStaticReferences.MyInvieRef(RoomActivity.this,item));
        /*DatabaseReference inviteRef = database.getReference(getApplicationContext().getResources()
                .getString(R.string.MyInvRef, item.UserID));*/
        inviteRef.child(roomName).setValue(roomPass);
        DatabaseReference invitedlist = database.getReference(DatabaseStaticReferences.RoomMembersDirectRef
                (RoomActivity.this,
                        roomName,
                        RoomActivity.this.getResources().getString(R.string.invitedFreinds),
                        item.UserID));
        invitedlist.setValue("1");
    }

    private void StartService() {
        Intent intent = new Intent(this, LocationService.class);
        intent.putExtra("roomName", roomName);
        intent.putExtra("userName", FirebaseAuth.getInstance().getCurrentUser().getDisplayName());
        startService(intent);
    }
}
