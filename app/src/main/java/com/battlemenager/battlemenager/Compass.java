package com.battlemenager.battlemenager;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import static android.content.Context.SENSOR_SERVICE;

/**
 * Created by Marcin on 2018-03-23.
 */

public class Compass extends Thread{
    private final static String TAG = "Compass";
    private SensorManager sensorManager;
    private Sensor magnetometer, accelerometer;
    private SensorEventListener sensorEventListener;
    private CompassListener compassListener;
    private float[] gravity = new float[3], geomagnetic = new float[3];
    private float alpha = 0.97f;

    public Compass(Context context, CompassListener compassListener) {
        this.compassListener = compassListener;
        sensorManager = (SensorManager) context.getSystemService(SENSOR_SERVICE);
        magnetometer = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    @Override
    public void run() {
        sensorEventListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                switch (event.sensor.getType()){
                    case Sensor.TYPE_ACCELEROMETER:
                        gravity[0] = alpha * gravity[0] + (1 - alpha) * event.values[0];
                        gravity[1] = alpha * gravity[1] + (1 - alpha) * event.values[1];
                        gravity[2] = alpha * gravity[2] + (1 - alpha) * event.values[2];
                        break;
                    case Sensor.TYPE_MAGNETIC_FIELD:
                        geomagnetic[0] = alpha * geomagnetic[0] + (1 - alpha) * event.values[0];
                        geomagnetic[1] = alpha * geomagnetic[1] + (1 - alpha) * event.values[1];
                        geomagnetic[2] = alpha * geomagnetic[2] + (1 - alpha) * event.values[2];
                        break;
                }
                float R[] = new float[9];
                float I[] = new float[9];

                if (SensorManager.getRotationMatrix(R, I, gravity, geomagnetic)){
                    float[] orientation = new float[3];
                    SensorManager.getOrientation(R, orientation);
                    float azimuth = (float) Math.toDegrees(orientation[0]);
                    azimuth = (azimuth + 360) % 360;
                    compassListener.onCompassResult(azimuth);
                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };
        sensorManager.registerListener(sensorEventListener, magnetometer, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(sensorEventListener, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

//    @Override
//    public boolean isInterrupted() {
//        sensorManager.unregisterListener(sensorEventListener, magnetometer);
//        sensorManager.unregisterListener(sensorEventListener, accelerometer);
//        return super.isInterrupted();
//    }

    public interface CompassListener {
        void onCompassResult(float azimuth);
    }
}
