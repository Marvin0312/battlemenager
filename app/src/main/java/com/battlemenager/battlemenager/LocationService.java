package com.battlemenager.battlemenager;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Marcin on 2018-02-21.
 */

public class LocationService extends Service {

    private final static String TAG = "LocationService";
    private LocationRequest locationRequest;
    private FusedLocationProviderClient fusedLocation;
    private LocationCallback locationCallback;
    private DatabaseReference reference;

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate: ");
        fusedLocation = LocationServices.getFusedLocationProviderClient(this);
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            fusedLocation = LocationServices.getFusedLocationProviderClient(this);
            String roomName = intent.getStringExtra("roomName");
            String userName = intent.getStringExtra("userName");
            reference = FirebaseDatabase.getInstance()
                    .getReference("BattleMenager")
                    .child("Room").child(roomName)
                    .child("members").child(userName);
            StartLocationUpdates();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        fusedLocation.removeLocationUpdates(locationCallback);
        reference.removeValue();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @SuppressWarnings("MissingPermission")
    private void StartLocationUpdates() {
        Log.d(TAG, "StartLocationUpdates: ");
        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(3000);
        locationRequest.setFastestInterval(1000);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(locationRequest);
        LocationSettingsRequest settingsRequest = builder.build();

        SettingsClient settingsClient = LocationServices.getSettingsClient(this);
        settingsClient.checkLocationSettings(settingsRequest);

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                Log.d(TAG, "onLocationResult: ");
                super.onLocationResult(locationResult);
                Location location = locationResult.getLastLocation();
                //  RoomActivity.MyLastLocation = new LatLng(location.getLatitude(),location.getLongitude());
                String latLng = location.getLatitude() + "|" + location.getLongitude();
                reference.setValue(latLng);

            }
        };

        fusedLocation.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
    }
}
